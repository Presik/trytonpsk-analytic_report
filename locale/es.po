#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:analytic_report.invoice.start,company:"
msgid "Company"
msgstr "Empresa"

msgctxt "field:analytic_report.invoice.start,end_date:"
msgid "End Date"
msgstr "Fin"

msgctxt "field:analytic_report.invoice.start,start_date:"
msgid "Start Date"
msgstr "Inicio"

msgctxt "field:analytic_report.invoice.start,type_inv:"
msgid "Type"
msgstr "Tipo"

msgctxt "field:analytic_report.reporting.start,detailed:"
msgid "Analytic Account Detailed"
msgstr ""

msgctxt "model:analytic_report.invoice.start,name:"
msgid "Analytic Invoice Report Start"
msgstr "Facturación Con Cuentas Analíticas"

msgctxt "model:analytic_report.reporting.start,name:"
msgid "Analytic Report Start"
msgstr ""

msgctxt "model:ir.action,name:report_analytic_account"
msgid "Analytic Account"
msgstr "Cuenta analítica"

msgctxt "model:ir.action,name:report_analytic_line_detailed"
msgid "Report Detailed"
msgstr "Reporte Cuentas analiticas Detallado"

msgctxt "model:ir.action,name:report_invoices_analytic"
msgid "Invoices Analytic"
msgstr "Plan Cuentas Analiticas"

msgctxt "model:ir.action,name:wizard_analytic_account_reporting"
msgid "Analytic Account Reporting"
msgstr "Plan Cuentas Analiticas"

msgctxt "model:ir.action,name:wizard_print_invoices_analytic"
msgid "Analytic Invoices"
msgstr "Plan Cuentas Analiticas"

msgctxt "model:ir.ui.menu,name:menu_print_invoices_analytic"
msgid "Analytic Invoices"
msgstr "Plan Cuentas Analiticas"

msgctxt "report:analytic_account.account_report:"
msgid "$"
msgstr "$"

msgctxt "report:analytic_account.account_report:"
msgid "("
msgstr "("

msgctxt "report:analytic_account.account_report:"
msgid ")"
msgstr ")"

msgctxt "report:analytic_account.account_report:"
msgid ","
msgstr ","

msgctxt "report:analytic_account.account_report:"
msgid "-"
msgstr "-"

msgctxt "report:analytic_account.account_report:"
msgid "/"
msgstr "/"

msgctxt "report:analytic_account.account_report:"
msgid "/for"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "/if"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "00/00/0000"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "00:00:00"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "???"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "COD."
msgstr ""

#, fuzzy
msgctxt "report:analytic_account.account_report:"
msgid "CREDITO"
msgstr "CREDITO"

msgctxt "report:analytic_account.account_report:"
msgid "CUENTA"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "CUENTAS ANALITICAS"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "DEBITO"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "Impresión:"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "Página"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "SALDO"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "account['account'].balance"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "account['account'].code"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "account['account'].credit"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "account['account'].debit"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "account['account'].name"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "company.party.name"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "for each=\"line in account['lines']\""
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "for each='account in records'"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "format_date(datetime.date.today(), company.party.lang)"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "if test=\"data['detailed']\""
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "line['account']"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "line['code']"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "sum(line['balance'])"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "sum(line['credit'])"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "sum(line['debit'])"
msgstr ""

msgctxt "report:analytic_account.account_report:"
msgid "€"
msgstr ""

#, fuzzy
msgctxt "report:analytic_account.line.detailed_report:"
msgid "$"
msgstr "$"

#, fuzzy
msgctxt "report:analytic_account.line.detailed_report:"
msgid "("
msgstr "("

#, fuzzy
msgctxt "report:analytic_account.line.detailed_report:"
msgid ")"
msgstr ")"

#, fuzzy
msgctxt "report:analytic_account.line.detailed_report:"
msgid ","
msgstr ","

#, fuzzy
msgctxt "report:analytic_account.line.detailed_report:"
msgid "-"
msgstr "-"

#, fuzzy
msgctxt "report:analytic_account.line.detailed_report:"
msgid "/"
msgstr "/"

msgctxt "report:analytic_account.line.detailed_report:"
msgid "/for"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "00/00/0000"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "00:00:00"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "???"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "ASIENTO"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "CENTRO OPERACIONES"
msgstr ""

#, fuzzy
msgctxt "report:analytic_account.line.detailed_report:"
msgid "CREDITO"
msgstr "CREDITO"

msgctxt "report:analytic_account.line.detailed_report:"
msgid "CUENTA"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "DEBITO"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "DESCRIPCION"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "DETALLE DE CUENTAS ANALITICAS"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "DOC. TERCERO"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "Impreso el:"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "NOMBRE CUENTA"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "ORIGEN"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "Página"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "SALDO"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "TERCERO"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "company.party.name"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "company.time_now()"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "for each='line in rec['lines']'"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "for each='line in records'"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['credit']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['debit']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['debit']-line['credit']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['move_line.']['account.']['code']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['move_line.']['account.']['name']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['move_line.']['description']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['move_line.']['move.']['number']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['move_line.']['move_origin.']['rec_name']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['move_line.']['operation_center.']['rec_name']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['move_line.']['party.']['id_number']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "line['move_line.']['party.']['name']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "rec['balance']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "rec['code']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "rec['credit']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "rec['debit']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "rec['name']"
msgstr ""

msgctxt "report:analytic_account.line.detailed_report:"
msgid "€"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "$"
msgstr "$"

msgctxt "report:analytic_report.invoice.report:"
msgid "("
msgstr "("

msgctxt "report:analytic_report.invoice.report:"
msgid "($"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid ")"
msgstr ")"

msgctxt "report:analytic_report.invoice.report:"
msgid ","
msgstr ","

msgctxt "report:analytic_report.invoice.report:"
msgid "-"
msgstr "-"

msgctxt "report:analytic_report.invoice.report:"
msgid "- pta"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "--"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "--)"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "/"
msgstr "/"

msgctxt "report:analytic_report.invoice.report:"
msgid "/for"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "00/00/0000"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "00:00:00"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid ":"
msgstr ":"

msgctxt "report:analytic_report.invoice.report:"
msgid "???"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "BASE"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "CENTRO DE COSTO"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "CENTROS DE COSTOS"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "COMPAÑIA:"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "CUENTA"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "DESCRIPCIÓN PRODUCTO"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "ESTADO FACTURA"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "FECHA"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "FECHAS:"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "IMPUESTOS"
msgstr "IMPUESTOS"

msgctxt "report:analytic_report.invoice.report:"
msgid "INFORME DE FACTURACIÓN POR"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "IVA"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "No FACTURA"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "OTROS"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "Page"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "RETENCIÓN"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "TERCERO"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "al"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "company.party.name"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "de"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "end"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "for each='rec in records'"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "pta"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['analytic']"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['iva']"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['line'].account.rec_name"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['line'].invoice.invoice_date"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['line'].invoice.number"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['line'].invoice.party.name"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['line'].invoice.state_string"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['line'].product.name"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['line'].unit_price * rec['line'].quantity"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['other_tax']"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "rec['renta']"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "start"
msgstr ""

msgctxt "report:analytic_report.invoice.report:"
msgid "€"
msgstr ""

msgctxt "selection:analytic_report.invoice.start,type_inv:"
msgid "In Credit Note"
msgstr "Nota Crédito de Compra"

msgctxt "selection:analytic_report.invoice.start,type_inv:"
msgid "Invoice"
msgstr "Factura de Venta"

msgctxt "selection:analytic_report.invoice.start,type_inv:"
msgid "Out Credit Note"
msgstr "Nota Crédito de Venta"

msgctxt "selection:analytic_report.invoice.start,type_inv:"
msgid "Supplier Invoice"
msgstr "Factura de Proveedor"

msgctxt "wizard_button:analytic_report.invoice,start,end:"
msgid "Cancel"
msgstr "Cancelar"

msgctxt "wizard_button:analytic_report.invoice,start,print_:"
msgid "Print"
msgstr "Imprimir"

msgctxt "wizard_button:analytic_report.reporting_wizard,start,end:"
msgid "Cancel"
msgstr "Cancelar"

msgctxt "wizard_button:analytic_report.reporting_wizard,start,print_:"
msgid "Print"
msgstr "Imprimir"
