#This file is part of Tryton. The COPYRIGHT file at the top level
#of this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from decimal import Decimal
from trytond.pool import Pool
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport


_ZERO = Decimal('0.0')


class AnalyticInvoiceStart(ModelView):
    'Analytic Invoice Report Start'
    __name__ = 'analytic_report.invoice.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    type_inv = fields.Selection([
            ('out', 'Invoice'),
            ('out_credit_note', 'Out Credit Note'),
            ('in', 'Supplier Invoice'),
            ('in_credit_note', 'In Credit Note'),
        ], 'Type', required=True)
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class AnalyticInvoice(Wizard):
    'Analytic Invoice Report'
    __name__ = 'analytic_report.invoice'
    start = StateView('analytic_report.invoice.start',
        'analytic_report.analytic_invoice_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('analytic_report.invoice.report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'type_invoice': self.start.type_inv,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            }
        return action, data

    def transition_print_(self):
        return 'end'


class AnalyticInvoiceReport(Report):
    __name__ = 'analytic_report.invoice.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Lines = pool.get('account.invoice.line')
        LineTax = pool.get('account.invoice.line-account.tax')
        if data['type_invoice'] == "out_credit_note":
            type_invoice = 'out'
        elif data['type_invoice'] == "in_credit_note":
            type_invoice = 'in'
        else:
            type_invoice = data['type_invoice']

        lines = Lines.search([
            ('invoice.company', '=', data['company']),
            ('invoice.type', '=', type_invoice),
            ('invoice.invoice_date', '>=', data['start_date']),
            ('invoice.invoice_date', '<=', data['end_date']),
            ('invoice.state', 'in', ['validated', 'posted', 'paid']),
        ])

        invoice_lines_filtered = []

        def get_parent_names(account, list_names, names=''):
            if account and account.parent:
                list_names.append(account.parent.name)
                get_parent_names(account.parent, list_names)
            i = len(list_names) - 1
            for l in list_names:
                names += list_names[i] + ' - '
                i -= 1
            return names

        for line in lines:
            lines_taxes = LineTax.search([
                ('line', '=', line.id)
            ])
            _line = {'line': line}
            iva_amount = 0
            renta_amount = 0
            other_amount = 0
            for line_tax in lines_taxes:
                if line_tax.tax.classification:
                    if line_tax.tax.classification == 'iva':
                        iva_amount = line.unit_price * line_tax.tax.rate
                    elif line_tax.tax.classification == 'renta':
                        renta_amount = line.unit_price * line_tax.tax.rate
                    elif line_tax.tax.type == 'percentage' and line_tax.tax.rate:
                        other_amount += line.unit_price * line_tax.tax.rate
                    elif line_tax.tax.rate:
                        other_amount += line_tax.tax.rate
                    else:
                        other_amount += 0
                else:
                    if line_tax.tax.type == 'fixed' and line_tax.tax.rate:
                        other_amount += line_tax.tax.rate
                    if line_tax.tax.rate and line_tax.tax.type == 'percentage':
                        other_amount += line.unit_price * line_tax.tax.rate
                    else:
                        other_amount += 0
            _line['iva'] = iva_amount
            _line['renta'] = renta_amount
            _line['other_tax'] = other_amount

            name_analytic = ''
            code = ''
            if line.analytic_accounts:
                for a in line.analytic_accounts:
                    code_name = ''
                    if a.account:
                        code = a.account.code
                        code_name = get_parent_names(a.account, [a.account.name])
                    name_analytic = name_analytic + code_name
            _line['analytic'] = name_analytic
            _line['analytic_code'] = code

            if line.invoice.untaxed_amount < _ZERO:
                if "credit" in data['type_invoice']:
                    invoice_lines_filtered.append(_line)
                else:
                    continue
            else:
                if "credit" not in data['type_invoice']:
                    invoice_lines_filtered.append(_line)
                else:
                    continue

        report_context['records'] = invoice_lines_filtered
        report_context['company'] = Company(data['company'])
        report_context['start'] = data['start_date']
        report_context['end'] = data['end_date']

        return report_context
