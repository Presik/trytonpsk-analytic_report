# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateReport, StateView, Button
from trytond.model import ModelView, fields


class AnalyticReportStart(ModelView):
    'Analytic Report Start'
    __name__ = 'analytic_report.reporting.start'
    detailed = fields.Boolean('Analytic Account Detailed')


class AnalyticAccountWizard(Wizard):
    'Analytic Account Wizard'
    __name__ = 'analytic_report.reporting_wizard'
    start = StateView('analytic_report.reporting.start',
        'analytic_report.analytic_report_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok'),
            ])
    print_ = StateReport('analytic_account.account_report')

    def do_print_(self, action):
        id_ = Transaction().context['active_id']
        start_date = Transaction().context['start_date']
        end_date = Transaction().context['end_date']
        detailed = self.start.detailed
        data = {
            'start_date': start_date,
            'end_date': end_date,
            'active_id': id_,
            'detailed': detailed,
        }

        return action, data

    def transition_print_(self):
        return 'end'


class AnalyticAccountReport(Report):
    __metaclass__ = PoolMeta
    __name__ = "analytic_account.account_report"

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        AnalyticAccount = Pool().get('analytic_account.account')
        Company = Pool().get('company.company')
        Line = Pool().get('analytic_account.line')
        with Transaction().set_context(data):
            records = [AnalyticAccount(data['active_id'])]
        targets = []
        while records:
            accounts = records.pop()
            if not isinstance(accounts, tuple):
                accounts = [accounts]
            for account in accounts:

                lines_ = []

                if account.childs:
                    records.append(account.childs)
                else:
                    dom = [('account', '=', account.id), ]
                    if data['start_date']:
                        dom.append(('date', '>=', data['start_date']),)
                    if data['end_date']:
                        dom.append(('date', '<=', data['end_date']),)
                    lines = Line.search_read(dom, fields_names=[
                        'move_line.account',
                        'move_line.account.code',
                        'move_line.account.name',
                        'move_line.debit',
                        'move_line.credit',
                    ])
                    if lines:
                        grouped = {}
                        for line in lines:
                            if line['move_line.']['account.']['code'] not in grouped.keys():
                                grouped[line['move_line.']['account.']['code']] = {
                                    'code': line['move_line.']['account.']['code'],
                                    'account': line['move_line.']['account.']['name'],
                                    'debit': [],
                                    'credit': [],
                                    'balance': [],
                                }
                            debit = line['move_line.']['debit']
                            credit = line['move_line.']['credit']
                            balance = debit - credit
                            grouped[line['move_line.']['account.']['code']]['balance'].append(
                                balance
                            )
                            grouped[line['move_line.']['account.']['code']]['credit'].append(
                                credit
                            )
                            grouped[line['move_line.']['account.']['code']]['debit'].append(
                                debit
                            )
                        lines_ = grouped.values()
                value = {
                    'account': account,
                    'lines': lines_
                    }
                targets.append(value)
        report_context['records'] = targets
        report_context['company'] = Company(Transaction().context.get('company'))
        report_context['data'] = data
        return report_context


class LineDetailedReport(Report):
    "Line Detailed Report "
    __name__ = "analytic_account.line.detailed_report"

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Line = Pool().get('analytic_account.line')
        AnalyticAccount = Pool().get('analytic_account.account')
        MoveLine = Pool().get('account.move.line')
        Company = Pool().get('company.company')
        context = Transaction().context
        company = Company(context['company'])
        dom = []

        if context['start_date']:
            dom.append(('date', '>=', context['start_date']),)
        if context['end_date']:
            dom.append(('date', '<=', context['end_date']),)

        fields_names = [
            'move_line.account',
            'move_line.account.code',
            'move_line.account.name',
            'debit',
            'credit',
            'move_line.move_origin.rec_name',
            'move_line.description',
            'move_line.reference',
            'move_line.party.name',
            'move_line.party.id_number',
            'move_line.move.number',
            'move_line.move.description',
            'move_line.move.date',
        ]

        fields = MoveLine.fields_get(fields_names=['operation_center'])
        if 'operation_center' in fields.keys():
            fields_names.append('move_line.operation_center.rec_name')

        accounts = AnalyticAccount.search_read([
            ('id', 'in', data['ids'])
        ], fields_names=['name', 'code', 'debit', 'credit', 'balance'])

        records = {p['id']: p for p in accounts}
        for d in data['ids']:
            domain = dom.copy()
            domain.append(('account', '=', d))
            lines = Line.search_read(domain, fields_names=fields_names)
            records[d]['lines'] = lines

        report_context['records'] = records.values()
        report_context['company'] = company
        report_context['context'] = context
        return report_context
