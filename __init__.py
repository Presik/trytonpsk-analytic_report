# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import analytic
from . import invoice


def register():
    Pool.register(
        # analytic.AnalyticAccountReport,
        # invoice.AnalyticInvoiceStart,
        # analytic.AnalyticReportStart,
        module='analytic_report', type_='model')
    Pool.register(
        # analytic.AnalyticAccountReport,
        # invoice.AnalyticInvoiceReport,
        # analytic.LineDetailedReport,
        module='analytic_report', type_='report')
    Pool.register(
        # analytic.AnalyticAccountWizard,
        # invoice.AnalyticInvoice,
        module='analytic_report', type_='wizard')
